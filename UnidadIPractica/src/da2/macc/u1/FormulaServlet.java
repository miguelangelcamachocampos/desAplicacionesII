package da2.macc.u1;

import java.*;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.filters.HttpHeaderSecurityFilter;
import org.apache.jasper.tagplugins.jstl.core.Out;

import com.sun.xml.internal.messaging.saaj.packaging.mime.Header;

import da2.macc.u1.Form;

/**
 * Servlet implementation class FormulaServlet
 */
@WebServlet("/FormulaServlet")
public class FormulaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormulaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
		double x = Double.parseDouble(request.getParameter("x"));

		Form form = new Form();
		form.setX(x);

		request.setAttribute("op", form);
		RequestDispatcher dp = request.getRequestDispatcher("result.jsp");
		dp.forward (request, response);
		}catch (NumberFormatException e){
			String out = "Caracteres no permitidos";
			java.lang.System.out.println(out);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
